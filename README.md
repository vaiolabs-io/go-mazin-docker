# Go Mazin Docker

Identical project of [Mazin-Docker](https://gitlab.com/vaiolabs-io/mazin-docker) implemented in Go for learning the language and it perks


Go-Mazin-Docker
Copyright (C) 2023  VaioLabs IO

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

---
The image belongs to [to pinterest user robot hell](https://www.pinterest.com.mx/pin/457185799664518683/)
